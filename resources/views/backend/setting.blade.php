@extends ('backend.layouts.app')

@section('content')
<div class="container">
    @if(Session::has('status'))
        <div class="alert alert-info">
            <span>{{ Session::get('status') }}</span>
        </div>
    @endif
    <form action="{{ route('admin.setting.store') }}" method="post">
         <div class="form-group">
             <label for="">Url callback for Bot</label>
             <div class=input-group>
                 <div class="input-group-btn">
                     <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                         Event <span class="caret"></span>
                     </button>
                     <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                         <li class="dropdown-item"><a href="#" onclick="document.getElementById('url_callback_bot').value = {{ url('') }}">Paste Url</a></li>
                         <li class="dropdown-item"><a href="#" onclick="event.preventDefault(); document.getElementById('setWebHook').submit()" >Send Url</a></li>
                         <li class="dropdown-item"><a href="#" onclick="event.preventDefault(); document.getElementById('getWebHookInfo').submit()">Get Information</a></li>
                     </ul>
                 </div>
                 <input type="text" class="form-control" id="url_callback_bot" name="url_callback_bot" value="{{ $url_callback_bot ?? '' }}" />
             </div>
         </div>
        @csrf
        <button class="btn btn-primary" type="submit">Save</button>
    </form>
    <form id="setWebHook" action="{{ route('admin.setting.setwebhook') }}" method="POST" style="display:none">
        @csrf
        <input type="hidden" name="url" value="{{ $url_callback_bot ?? '' }}" />
    </form>
      <form id="getWebHookInfo" action="{{ route('admin.setting.getwebhookinfo') }}" method="POST" style="display:none">
        @csrf
    </form>
</div>

@endsection
@extends('backend.layouts.app')

@section('content')
    <task-component :task="{!! json_encode($task) !!}"></task-component>

@endsection
<?php

namespace App\Telegram;

/**
 * Class TestCommand.
 */

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

class TestCommand extends Command
{
    protected $name = "start";

    /**
     * @var string Command Description
     */
    protected $description = "Начало положено";

    /**
     * @inheritdoc
     */

    public function handle($arguments)
    {
        $this->replyWithChatAction(['action' => Actions::TYPING]);

        $telegram_user = \Telegram::getWebhookUpdates()['message'];
        $keyboard = [
            ["Хочу фильм"],
        ];
        $reply_markup = \Telegram::replyKeyboardMarkup([
            'keyboard' => $keyboard,
            'resize_keyboard' => true,
            'one_time_keyboard' => false
        ]);

        $response = \Telegram::sendMessage([
            'chat_id' => $telegram_user['from']['id'],
            'text' => 'Привет, если хочешь посмотреть фильм просто нажми на кнопку и я подскажу тебе',
            'reply_markup' => $reply_markup
        ]);

        $messageId = $response->getMessageId();

    }
}


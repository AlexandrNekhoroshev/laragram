<?php

namespace App\Console\Commands;

use App\Backend\Messages;
use App\Events\ChatMessageWasReceived;
use App\User;
use Illuminate\Console\Command;

class SendChatMessage extends Command
{
    protected $signature = 'chat:message {message}';

    protected $description = 'Send chat message.';

    public function handle()
    {
        $user = User::first();
        $message = Messages::create([
            'user_id' => $user->id,
            'message' => $this->argument('message')
        ]);

        event(new ChatMessageWasReceived($message, $user));
    }
}
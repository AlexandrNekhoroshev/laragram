<?php

namespace App\Backend;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
    protected $fillable = ['message', 'customer_id'];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

<?php

namespace App\Http\Controllers\Backend;

use App\Events\ChatMessageWasReceived;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Telegram\Bot\Api;
use Telegram\Bot\Laravel\Facades\Telegram;

class BotController extends Controller
{

    protected $telegram;

    public function __construct(Api $telegram)
    {
        $this->telegram = $telegram;
    }
    public function command(Request $request)
    {
        $update = $this->telegram->commandsHandler(true);
        $message = $update->getMessage();
        $user = new User();
//        if (str_contains($message->getText(), ['Хочу фильм'])) {
//            $this->telegram->sendChatAction(['action' => 'typing', 'chat_id' => $message->getChat()->getId()]);
//            for ($i = 1; $i <= 999; $i++) {
//                $endpoint = 'http://api.themoviedb.org/3/movie/' . rand(1, 999);
//                $client = new \GuzzleHttp\Client();
//                $result = $client->get($endpoint, ['query' => [
//                    'api_key' => '910f3a38f0d5aa1263fc8915b1461c5e',
//                    'language' => 'ru',
//                ]]);
//                $content = json_decode($result->getBody()->getContents());
//                if (!empty($content->belongs_to_collection->name)) {
//                    $this->telegram->sendMessage(['text' => $content->belongs_to_collection->name, 'chat_id' => $message->getChat()->getId()]);
//                    $this->telegram->sendPhoto(['chat_id' => $message->getChat()->getId(), 'photo' => 'https://image.tmdb.org/t/p/w500'.$content->belongs_to_collection->poster_path]);
//                    break;
//                }
//            }
//        }

//        $text =  $user->messages()->create([
//            'customer_id' => $message->getChat()->getId(),
//            'message' => $message->getText()
//        ]);
//        $this->telegram->sendMessage(['text' => 'Ожидайте, скоро с вами свяжется наш сотрудник', 'chat_id' => $message->getChat()->getId()]);
        event(new ChatMessageWasReceived($update->getMessage()->getText(), $update->getMessage()->getChat()->getId()));

    }

    public function send_answer(Request $request){
         $this->telegram->sendMessage(['text' => $request->input('message'), 'chat_id' => $request->input('chat_id')]);
    }
}


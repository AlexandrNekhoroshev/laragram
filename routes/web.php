<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::middleware(['auth'])->prefix('admin')->namespace('Backend')->name('admin.')->group(function(){
    Route::get('/', 'DashboardController@index')->name('index');
    Route::get('/chat', 'ChatController@index')->name('chat.index');
    Route::get('/setting', 'SettingController@index')->name('setting.index');
    Route::post('/setting/store', 'SettingController@store')->name('setting.store');
    Route::post('/setting/setwebhook', 'SettingController@setwebhook')->name('setting.setwebhook');
    Route::post('/setting/getwebhookinfo', 'SettingController@getwebhookinfo')->name('setting.getwebhookinfo');
    Route::post ('/message_send', 'BotController@send_answer')->name('send_answer');
    Route::get('/task', function (){

        return view('backend.task.index', [
            'task' => array(1,2,3,4,6,7)
        ]);
    })->name('task');
});

Auth::routes();

Route::post('/' . config('telegram.bot_token'), 'Backend\BotController@command');



Route::match(['get','post'], 'register', function(){
    Auth::logout();
    return redirect('/');
})->name('register');

Route::get('/home', 'HomeController@index')->name('home');
